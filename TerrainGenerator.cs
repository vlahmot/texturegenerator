﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ThePostDays
{
    public class TerrainGenerator
    {
        System.Random random = new System.Random();

        /// <summary>
        /// Generates a 2 dimensional height map of size 2^n using the Diamond-Square algorithm.
        /// </summary>
        /// <param name="n"> </param>
        /// <returns></returns>
        public float[,] GenerateHeightMap(int n)
        {
            float randLower = -20;
            float randUpper = 20;
            float noise = .01f;

            //Dimensions must be a power of 2 + 1
            int dim = (int)(Math.Pow(2, n) + 1);
            int origDim = dim;

            float[,] array = new float[dim, dim];

            Set4Corners(array, 0, 0, dim - 1, randLower, randUpper);


            float modNoise;

            for (int i = origDim; i > 2; i = (int)Math.Ceiling(i / 2.0f))
            {
                //Sets modified noise for this level
                modNoise = (randUpper - randLower) * noise * ((float)i / origDim);

                for (int j = 0; j < origDim - i + 1; j += i - 1)
                {
                    for (int k = 0; k < origDim - i + 1; k += i - 1)
                    {
                        Diamond(array, j, k, i - 1, RandRange(random, randLower, randUpper));
                    }
                }

                for (int j = 0; j < origDim - i + 1; j += i - 1)
                {
                    for (int k = 0; k < origDim - i + 1; k += i - 1)
                    {
                        Square(array, j, k, i - 1, RandRange(random, randLower, randUpper), new Vector2(origDim, origDim));
                    }
                }
            }
            return array;
        }

        /// <summary>
        /// Sets the four corners of a square in the array to random values.
        /// </summary>
        /// <param name="array">The array to be modified.</param>
        /// <param name="x">The x coordinate to start at.</param>
        /// <param name="y">The y coordinate to start at.</param>
        /// <param name="length">The length of the square.</param>
        /// <param name="randL">A low random modifier.</param>
        /// <param name="randH">A high random modifier.</param>
        public void Set4Corners(float[,] array, int x, int y, int length, float randL, float randH)
        {
            array[x, y] = RandRange(random, randL, randH);
            array[x + length, y + length] = RandRange(random, randL, randH);
            array[x, y + length] = RandRange(random, randL, randH);
            array[x + length, y] = RandRange(random, randL, randH);
        }

        /// <summary>
        /// Sets the middle node in a square to the average of it's four corners plus a random amount.
        /// </summary>
        /// <param name="array">The array to be modified.</param>
        /// <param name="x">The x coordinate to start at.</param>
        /// <param name="y">The y coordinate to start at.</param>
        /// <param name="length">The length of the square.</param>
        /// <param name="randMod"> The random modifier to be added on.</param>
        public void Diamond(float[,] array, int x, int y, int length, float randMod)
        {
            array[x + length / 2, y + length / 2] = (float)(AverageOfFourCorners(array, x, y, length, randMod) + randMod);
        }

        /// <summary>
        /// Sets the four midpoints along the sides of a square. 
        /// The value is the average of all perpendicular points plus a random modifer.
        /// </summary>
        /// <param name="array">The array to be modified.</param>
        /// <param name="x">The x coordinate to start at.</param>
        /// <param name="y">The y coordinate to start at.</param>
        /// <param name="length">The length of the square.</param>
        /// <param name="randMod"> The random modifier to be added on.</param>
        /// <param name="dimension">The dimensions of the height map.</param>
        public void Square(float[,] array, int x, int y, int length, float randMod, Vector2 dimension)
        {
            float mid = array[x + length / 2, y + length / 2];

            float topLeft = array[x, y];
            float topRight = array[x + length, y];
            float bottomLeft = array[x, y + length];
            float bottomRight = array[x + length, y + length];

            // Top midpoint

            //At top of height map
            if (y == 0)
            {
                array[x + length / 2, y] = (float)(((topLeft + topRight + mid) / 3.0f) + randMod);
            }

            // Not at the top of height map, can reach into above square.
            else
            {
                array[x + length / 2, y] = (float)(((topLeft + topRight + mid + array[x + length / 2, y - length / 2]) / 4.0f) + randMod);
            }

            //Right midpoint
            if (x == dimension.X - 1)
            {
                array[x + length, y + length / 2] = (float)(((topRight + bottomRight + mid) / 3.0f) + randMod);
            }
            else
            {
                //Not edge point, can reach beyond
                int right = x + length + length / 2;
                if (right >= dimension.X)
                {
                    array[x + length, y + length / 2] = (float)(((topRight + bottomRight + mid) / 3.0f) + randMod);
                }
                else
                {
                    array[x + length, y + length / 2] = (float)(((topRight + bottomRight + mid + array[x + length + length / 2, y + length / 2]) / 4.0f) + randMod);
                }
            }

            //Bottom midpoint
            if (y == dimension.Y - 1)
            {
                array[x + length / 2, y + length] = (float)(((bottomRight + bottomLeft + mid) / 3.0f) + randMod);
            }
            else
            {
                int bottom;
                bottom = y + length + length / 2;
                if (bottom >= dimension.Y)
                {
                    array[x + length / 2, y + length] = (float)(((bottomRight + bottomLeft + mid) / 3.0f) + randMod);
                }
                else
                {
                    array[x + length / 2, y + length] = (float)(((bottomRight + bottomLeft + mid + array[x + length / 2, y + length + length / 2]) / 4.0f) + randMod);
                }
            }

            //Left midpoint
            if (x == 0)
            {
                array[x, y + length / 2] = (float)(((topLeft + bottomLeft + mid) / 3.0f) + randMod);
            }
            else
            {
                array[x, y + length / 2] = (float)(((topLeft + bottomLeft + mid + array[x - length / 2, y]) / 4.0f) + randMod);
            }
        }


        /// <summary>
        /// Determines the average of four corners of a square in a 2-dimensional array.
        /// </summary>
        /// <param name="array">The array to be modified.</param>
        /// <param name="x">The x coordinate to start at.</param>
        /// <param name="y">The y coordinate to start at.</param>
        /// <param name="length">The length of the square.</param>
        /// <param name="randMod"> The random modifier to be added on.</param>
        /// <returns></returns>
        public float AverageOfFourCorners(float[,] array, int x, int y, int length, float randMod)
        {
            float first = array[x, y];
            float second = array[x + length, y];
            float third = array[x + length, y + length];
            float fourth = array[x, y + length];

            return (float)(((first + second + third + fourth) / 4.0f));
        }

        /// <summary>
        /// Creates a grayscale texture based on a heightmap
        /// </summary>
        /// <param name="device">The graphics device used to create the texture.</param>
        /// <param name="heightMap"> Array representation of a height map.</param>
        /// <returns> Grayscale texture where the level of black determines height.</returns>
        public Texture2D GenerateTexture(GraphicsDevice device, float[,] heightMap)
        {
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);
            Texture2D tex = new Texture2D(device, width, height);
            Color[] colorArray = new Color[width * height];


            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    colorArray[(i * height) + j] = GetColorFromHeightMap(heightMap[i, j]);
                }
            }
            tex.SetData<Color>(colorArray);
            return tex;
        }

        /// <summary>
        /// Generates a grayscale color for a float value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Color GetColorFromHeightMap(float value)
        {
            Vector4 vect = new Vector4(NormalizeFloat(0, 255, value), NormalizeFloat(0, 255, value),
            NormalizeFloat(0, 255, value), 100.0f);

            //Magic numbers
            Color oldColor = new Color(vect);
            float grayValue = oldColor.R * 0.299f / 255.0f;
            grayValue += oldColor.G * 0.596f / 255.0f;
            grayValue += oldColor.B * 0.211f / 255.0f;
            float alpha = oldColor.A / 255.0f;
        

            return new Color(grayValue, grayValue, grayValue, alpha);
        }

        /// <summary>
        /// Generates a random number within the range (min,max).
        /// </summary>
        /// <param name="rand">A System.Random object to use.</param>
        /// <param name="min">Minimum value of the range, inclusive.</param>
        /// <param name="max">Maximun value of the range, inclusive.</param>
        /// <returns></returns>
        public float RandRange(Random rand, float min, float max)
        {
            return min + (float)rand.NextDouble() * (max - min);
        }

        /// <summary>
        /// Normalizes a float to a value in a range.
        /// </summary>
        /// <param name="lowD">The low value of the range.</param>
        /// <param name="highD">The high value of the range.</param>
        /// <param name="value">The number to be normalized in the range.</param>
        /// <returns></returns>
        public static float NormalizeFloat(float lowD, float highD, float value)
        {

            return (float)((value - lowD) / (highD - lowD));
        }
    }
}

